package main

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func signUp(c *gin.Context) {
	c.HTML(http.StatusOK, "signup.html", nil)
}

func signIn(c *gin.Context) {
	c.HTML(http.StatusOK, "signin.html", nil)
}

func signOut(c *gin.Context) {
	token, err := c.Cookie("token")

	// no token in cookie
	if err != nil {
		c.Redirect(http.StatusFound, "/?logout=1")
		return
	}

	// delete cookie
	c.SetCookie("token", "token", -1, "/", "", false, true)

	var student Student
	has, _ := engine.Where("token = ?", token).Get(&student)

	// no such user
	if !has {
		c.Redirect(http.StatusFound, "/?logout=1")
		return
	}

	// delete token in DB
	student.Token = "token"
	engine.Where("id = " + strconv.FormatInt(student.Id, 10)).Update(&student)

	c.Redirect(http.StatusFound, "/?logout=1")
}

func getUser(c *gin.Context) *Student {
	token, err := c.Cookie("token")

	// no token in cookie
	if err != nil {
		return nil
	}

	if token == "token" {
		return nil
	}

	student := Student{Token: token}
	has, _ := engine.Get(&student)

	// user not exist
	if !has {
		return nil
	}

	return &student
}
