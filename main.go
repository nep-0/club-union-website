package main

import (
	"time"

	"github.com/gin-gonic/gin"
	_ "github.com/mattn/go-sqlite3"
	"xorm.io/xorm"
)

var r = gin.Default()
var engine, _ = xorm.NewEngine("sqlite3", "data.db")

func main() {
	r.Delims("{{{", "}}}")
	r.LoadHTMLGlob("templates/*")
	r.Static("/static", "./static")
	// r.Use(cors.Default()) // allow all origins

	engine.TZLocation, _ = time.LoadLocation("Local")
	engine.DatabaseTZ, _ = time.LoadLocation("Local")

	engine.Sync(new(Student))
	engine.Sync(new(Admin))
	engine.Sync(new(Club), new(ClubSelection))
	engine.Sync(new(Product), new(Order))

	r.GET("/", func(c *gin.Context) {
		logout := ""
		if c.Query("logout") != "" {
			logout = "已退出登录！"
		}

		c.HTML(200, "main.html", gin.H{
			"content": "Homepage",
			"logout":  logout,
		})
	})

	r.POST("/signup", signUp)
	r.GET("/signup", func(c *gin.Context) {
		c.HTML(200, "signup.html", nil)
	})

	r.POST("/login", signIn)
	r.GET("/login", func(c *gin.Context) {
		c.HTML(200, "signin.html", nil)
	})

	r.GET("/logout", signOut)

	r.GET("/clubs", selectClub)
	r.GET("/info", query)

	r.GET("/shop_confirm", func(c *gin.Context) {
		c.HTML(200, "shop_confirm.html", nil)
	})
	r.GET("/shop", shop)
	r.GET("/buy", buy)

	r.GET("/admin", admin)
	r.POST("/admin", admin)
	r.GET("/exit", adminSignOut)
	r.GET("/newAdmin", newAdmin)
	r.POST("/newAdmin", newAdmin)
	r.GET("/checkOrder", checkOrder)
	r.GET("/rmStudent", rmStudent)
	r.GET("/addProduct", addProduct)
	r.GET("/editProduct", editProduct)
	r.GET("/rmProduct", rmProduct)
	r.GET("/addClub", addClub)
	r.GET("/editClub", editClub)
	r.GET("/rmClub", rmClub)
	r.GET("/get", getSelectionCSV)
	r.GET("/deliver", deliver)

	r.POST("/api/signup", Register)
	r.POST("/api/signin", Login)
	r.GET("/api/info", Info)
	r.GET("/api/getClubs", GetClubs)
	r.GET("/api/selectClub", Select)
	r.GET("/api/getProducts", GetProducts)
	r.GET("/api/purchase", Purchase)
	r.GET("/api/cancelOrder", CancelOrder)

	r.Run()
}
