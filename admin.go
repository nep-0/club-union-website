package main

import (
	"os"
	"strconv"
	"sync"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

var clubSelectionLock sync.RWMutex
var orderLock sync.RWMutex

func admin(c *gin.Context) {
	password := c.PostForm("password")
	from := c.Query("from")
	if password == "" {
		if from == "" {
			c.HTML(200, "admin.html", nil)
			return
		}
		c.HTML(200, "admin.html", from)
		return
	}
	from = c.PostForm("from")

	admin := &Admin{}
	has, _ := engine.Get(admin)

	if !has {
		admin.Password = MD5(password)

		token := uuid.New().String()
		c.SetCookie("adminToken", token, 1800, "/", "", false, true)
		admin.Token = token

		engine.Insert(admin)

		c.String(201, "Welcome first admin!")
		return
	}

	admin.Password = MD5(password)
	has, _ = engine.Get(admin)
	if !has {
		c.HTML(401, "admin.html", nil)
		return
	}

	token := uuid.New().String()
	c.SetCookie("adminToken", token, 1800, "/", "", false, true)
	admin.Token = token

	engine.ID(admin.Id).Update(admin)

	if from == "" {
		c.String(200, "Welcome back!")
		return
	}
	c.Redirect(302, "/"+from)
}

func adminSignOut(c *gin.Context) {
	token, err := c.Cookie("adminToken")

	// no token in cookie
	if err != nil {
		c.String(200, "OK!")
		return
	}

	// delete cookie
	c.SetCookie("adminToken", "token", -1, "/", "", false, true)

	var admin Admin
	has, _ := engine.Where("token = ?", token).Get(&admin)

	// no such user
	if !has {
		c.String(200, "OK!")
		return
	}

	// delete token in DB
	admin.Token = "token"
	engine.Where("id = " + strconv.FormatInt(admin.Id, 10)).Update(&admin)

	c.String(200, "OK!")
}

func getAdmin(c *gin.Context) *Admin {
	token, err := c.Cookie("adminToken")

	// no token in cookie
	if err != nil {
		return nil
	}

	if token == "token" {
		return nil
	}

	admin := Admin{Token: token}
	has, _ := engine.Get(&admin)

	// user not exist
	if !has {
		return nil
	}

	return &admin
}

func newAdmin(c *gin.Context) {
	admin := getAdmin(c)
	if admin == nil {
		c.Redirect(302, "/admin?from=newAdmin")
		return
	}

	password := c.PostForm("password")
	if password == "" {
		c.HTML(200, "new_admin.html", nil)
		return
	}

	if password != admin.Password {
		c.String(401, "Wrong password!")
		return
	}

	new := c.PostForm("new")
	if new == "" {
		c.HTML(400, "new_admin.html", nil)
		return
	}

	engine.Insert(&Admin{Password: MD5(new), Token: "token"})
	c.String(201, "OK!")
}

func checkOrder(c *gin.Context) {
	admin := getAdmin(c)
	if admin == nil {
		c.Redirect(302, "/admin?from=checkOrder")
		return
	}

	code := c.Query("code")
	price := c.Query("price")

	if code == "" && price == "" {
		c.HTML(200, "check_order.html", nil)
		return
	}
	if code == "" || price == "" {
		c.String(401, "Empty feild!")
		return
	}
	if len(code) != 4 {
		c.String(401, "Wrong code!")
		return
	}
	priceNumber, err := strconv.ParseFloat(price, 32)
	if err != nil {
		c.String(401, "Wrong price!")
		return
	}

	order := &Order{Code: code}
	has, _ := engine.Get(order)
	if !has {
		c.HTML(401, "check_order.html", nil)
		return
	}
	if order.Status != OrderUnpaid {
		c.HTML(401, "check_order.html", nil)
		return
	}

	product := &Product{Id: order.Product}
	has, _ = engine.Get(product)
	if !has {
		c.HTML(401, "check_order.html", nil)
		return
	}
	if priceNumber != float64(product.Price) {
		c.HTML(401, "check_order.html", nil)
		return
	}

	order.Status = OrderPaid
	engine.ID(order.Id).Update(order)

	c.HTML(201, "check_order.html", nil)
}

func rmStudent(c *gin.Context) {
	admin := getAdmin(c)
	if admin == nil {
		c.Redirect(302, "/admin?from=rmStudent")
		return
	}

	studentID := c.Query("studentID")
	if studentID == "" {
		c.HTML(200, "rm_student.html", nil)
		return
	}

	id, err := strconv.ParseInt(studentID, 10, 64)
	if err != nil {
		c.String(400, "Wrong ID!")
		return
	}

	student := &Student{Id: id}
	has, _ := engine.Get(student)
	if has {
		engine.Delete(student)
		c.String(201, "OK!")
		return
	}

	c.String(400, "No such student.")
}

func addClub(c *gin.Context) {
	admin := getAdmin(c)
	if admin == nil {
		c.Redirect(302, "/admin?from=addClub")
		return
	}

	name := c.Query("name")
	custodian := c.Query("custodian")
	image := c.Query("image")
	contentHTML := c.Query("html")

	if name == "" && custodian == "" && image == "" && contentHTML == "" {
		c.HTML(200, "add_club.html", nil)
		return
	}

	if name == "" || image == "" {
		c.String(400, "Empty feild!")
		return
	}

	if custodian == "" {
		engine.Insert(&Club{
			Name:        name,
			Custodian:   int64(0),
			CoverImage:  image,
			ContentHTML: contentHTML,
		})
		c.String(201, "OK!")
		return
	}

	custodianID, err := strconv.ParseInt(custodian, 10, 64)
	if err != nil {
		c.String(400, "Wrong custodian!")
		return
	}

	student := &Student{Id: custodianID}
	has, _ := engine.Get(student)
	if has {
		engine.Insert(&Club{
			Name:        name,
			Custodian:   custodianID,
			CoverImage:  image,
			ContentHTML: contentHTML,
		})
	} else {
		engine.Insert(&Club{
			Name:        name,
			Custodian:   int64(0),
			CoverImage:  image,
			ContentHTML: contentHTML,
		})
	}

	c.String(201, "OK!")
}

func editClub(c *gin.Context) {
	admin := getAdmin(c)
	if admin == nil {
		c.Redirect(302, "/admin?from=editClub")
		return
	}

	if c.Query("id") == "" {
		c.HTML(200, "edit_club.html", nil)
		return
	}

	name := c.Query("name")
	custodian := c.Query("custodian")
	image := c.Query("image")
	contentHTML := c.Query("html")
	id, err := strconv.ParseInt(c.Query("id"), 10, 64)

	if err != nil {
		c.String(400, "Wrong ID!")
		return
	}
	club := &Club{Id: id}
	has, _ := engine.Get(club)
	if !has {
		c.String(400, "Wrong ID!")
		return
	}

	if custodian != "" {
		custodianID, err := strconv.ParseInt(custodian, 10, 64)
		if err != nil {
			c.String(400, "Wrong custodian!")
			return
		}
		student := &Student{Id: custodianID}
		has, _ = engine.Get(student)
		if has {
			club.Custodian = custodianID
		}
	}

	if image != "" {
		club.CoverImage = image
	}

	if contentHTML != "" {
		club.ContentHTML = contentHTML
	}

	if name != "" {
		club.Name = name
	}

	engine.ID(club.Id).Update(club)

	c.String(201, "OK!")
}

func rmClub(c *gin.Context) {
	admin := getAdmin(c)
	if admin == nil {
		c.Redirect(302, "/admin?from=rmClub")
		return
	}

	clubID := c.Query("clubID")
	if clubID == "" {
		c.HTML(200, "rm_club.html", nil)
		return
	}

	id, err := strconv.ParseInt(clubID, 10, 64)
	if err != nil {
		c.String(400, "Wrong ID!")
		return
	}

	club := &Club{Id: id}
	has, _ := engine.Get(club)
	if has {
		engine.Delete(club)
		c.String(201, "OK!")
		return
	}

	c.String(400, "No such product.")
}

func addProduct(c *gin.Context) {
	admin := getAdmin(c)
	if admin == nil {
		c.Redirect(302, "/admin?from=addProduct")
		return
	}

	name := c.Query("name")
	price := c.Query("price")
	image := c.Query("image")
	payCode := c.Query("payCode")

	if name == "" && price == "" && image == "" && payCode == "" {
		c.HTML(200, "add_product.html", nil)
		return
	}

	if name == "" || price == "" || image == "" || payCode == "" {
		c.String(400, "Empty feild!")
		return
	}

	priceNumber, err := strconv.ParseFloat(price, 32)
	if err != nil {
		c.String(400, "Wrong price!")
		return
	}

	engine.Insert(&Product{
		Name:    name,
		Image:   image,
		PayCode: payCode,
		Price:   float32(priceNumber),
	})

	c.String(201, "OK!")
}

func editProduct(c *gin.Context) {
	admin := getAdmin(c)
	if admin == nil {
		c.Redirect(302, "/admin?from=editProduct")
		return
	}

	if c.Query("id") == "" {
		c.HTML(200, "edit_product.html", nil)
		return
	}

	name := c.Query("name")
	image := c.Query("image")
	payCode := c.Query("payCode")
	price := c.Query("price")
	id, err := strconv.ParseInt(c.Query("id"), 10, 64)

	if err != nil {
		c.String(400, "Wrong ID!")
		return
	}
	product := &Product{Id: id}
	has, _ := engine.Get(product)
	if !has {
		c.String(400, "Wrong ID!")
		return
	}

	if payCode != "" {
		product.PayCode = payCode
	}

	if image != "" {
		product.Image = image
	}

	if price != "" {
		priceNumber, err := strconv.ParseFloat(price, 32)
		if err != nil {
			c.String(400, "Wrong price!")
			return
		}
		product.Price = float32(priceNumber)
	}

	if name != "" {
		product.Name = name
	}

	engine.ID(product.Id).Update(product)

	c.String(201, "OK!")
}

func rmProduct(c *gin.Context) {
	admin := getAdmin(c)
	if admin == nil {
		c.Redirect(302, "/admin?from=rmProduct")
		return
	}

	productID := c.Query("productID")
	if productID == "" {
		c.HTML(200, "rm_product.html", nil)
		return
	}

	id, err := strconv.ParseInt(productID, 10, 64)
	if err != nil {
		c.String(400, "Wrong ID!")
		return
	}

	product := &Product{Id: id}
	has, _ := engine.Get(product)

	if has {
		has, _ = engine.Get(&Order{Product: id, Status: OrderUnpaid})
		if has {
			c.String(401, "Cannot remove a product with an open order.")
			return
		}

		has, _ = engine.Get(&Order{Product: id, Status: OrderPaid})
		if has {
			c.String(401, "Cannot remove a product with an open order.")
			return
		}
		engine.Delete(product)
		c.String(201, "OK!")
		return
	}
	c.String(400, "No such product.")
}

func generateSelectionCSV() {
	var csv = "年级,班级,学号,姓名,选择社团,选择时间\n"
	var studentId, clubId int64
	var student *Student
	var club *Club

	clubSelections := make([]*ClubSelection, 0)
	engine.Find(&clubSelections)

	for _, v := range clubSelections {
		studentId = v.Student
		clubId = v.Club

		club = &Club{Id: clubId}
		student = &Student{Id: studentId}

		engine.Get(club)
		engine.Get(student)

		if student.Grade%10 == 0 {
			csv += "初 "
		} else {
			csv += "高 "
		}

		csv += strconv.FormatInt(int64(student.Grade), 10)[:4] + "," +
			strconv.FormatInt(int64(student.Class), 10) + "," +
			strconv.FormatInt(int64(student.Number), 10) + "," +
			student.Name + "," +
			club.Name + "," +
			v.Time.Format("2006-01-02 15:04:05") + "\n"
	}

	clubSelectionLock.Lock()
	os.Create("clubSelection.csv")
	f, _ := os.OpenFile("clubSelection.csv", 1, 0666)
	_, err := f.WriteString(csv)
	if err != nil {
		panic(err)
	}
	f.Close()
	clubSelectionLock.Unlock()
}

func getSelectionCSV(c *gin.Context) {
	admin := getAdmin(c)
	if admin == nil {
		c.Redirect(302, "/admin?from=get")
		return
	}

	generateSelectionCSV()
	clubSelectionLock.RLock()
	c.FileAttachment("clubSelection.csv", "clubSelection.csv")
	clubSelectionLock.RUnlock()
}

func generateOrderCSV() {
	var csv = "年级,班级,学号,姓名,识别码,商品\n"
	var studentId, productId int64
	var student *Student
	var product *Product

	orders := make([]*Order, 0)
	engine.Find(&orders, &Order{Status: OrderPaid})

	for _, v := range orders {
		studentId = v.Student
		productId = v.Product

		product = &Product{Id: productId}
		student = &Student{Id: studentId}

		engine.Get(product)
		engine.Get(student)

		engine.ID(v.Id).Update(&Order{Status: OrderDelivered})

		if student.Grade%10 == 0 {
			csv += "初 "
		} else {
			csv += "高 "
		}

		csv += strconv.FormatInt(int64(student.Grade), 10)[:4] + "," +
			strconv.FormatInt(int64(student.Class), 10) + "," +
			strconv.FormatInt(int64(student.Number), 10) + "," +
			student.Name + "," +
			v.Code + "," +
			product.Name + "\n"
	}

	orderLock.Lock()
	os.Create("orders.csv")
	f, _ := os.OpenFile("orders.csv", 1, 0666)
	_, err := f.WriteString(csv)
	if err != nil {
		panic(err)
	}
	f.Close()
	orderLock.Unlock()
}

func deliver(c *gin.Context) {
	admin := getAdmin(c)
	if admin == nil {
		c.Redirect(302, "/admin?from=deliver")
		return
	}

	generateOrderCSV()
	orderLock.RLock()
	c.FileAttachment("orders.csv", "orders.csv")
	orderLock.RUnlock()
}
