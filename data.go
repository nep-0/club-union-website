package main

import "time"

type OrderStatus = int

const (
	OrderUnpaid    OrderStatus = 0
	OrderPaid      OrderStatus = 1
	OrderDelivered OrderStatus = 2
)

type Student struct {
	Id       int64
	Grade    int16 // 入学年份+0（初中）或1（高中）
	Class    int8  // 1~8
	Number   int8  // 1~50
	Name     string
	Password string
	Token    string
}

type Club struct {
	Id          int64
	Name        string
	Custodian   int64 // 社团负责人（学生），0为未指定。
	CoverImage  string
	ContentHTML string
	Time        string
	Description string
}

type ClubSelection struct {
	Student int64     `xorm:"pk"`
	Time    time.Time `xorm:"updated"`
	Club    int64
}

type Product struct {
	Id      int64
	Name    string
	Image   string
	PayCode string
	Price   float32
}

type Order struct {
	Id      int64
	Student int64
	Product int64
	Code    string
	Time    time.Time `xorm:"created"`
	Status  OrderStatus
}

type Admin struct {
	Id       int64
	Password string
	Token    string
}
