package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func selectClub(c *gin.Context) {
	student := getUser(c)
	if student == nil {
		c.Redirect(http.StatusFound, "/login")
	}

	c.HTML(http.StatusOK, "clubs.html", nil)
}

func query(c *gin.Context) {
	student := getUser(c)
	if student == nil {
		c.Redirect(http.StatusFound, "/login")
		return
	}

	login := ""
	if c.Query("login") != "" {
		login = "登录成功！"
	}

	c.HTML(http.StatusOK, "info.html", login)
}

func shop(c *gin.Context) {
	student := getUser(c)
	if student == nil {
		c.Redirect(http.StatusFound, "/login")
		return
	}

	c.HTML(http.StatusOK, "shop.html", nil)
}

func buy(c *gin.Context) {
	student := getUser(c)
	if student == nil {
		c.Redirect(http.StatusFound, "/login")
		return
	}

	c.HTML(http.StatusOK, "buy.html", c.Query("product"))
}
