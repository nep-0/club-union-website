package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func MD5(str string) string {
	w := md5.New()
	io.WriteString(w, str)
	md5str := fmt.Sprintf("%x", w.Sum(nil))
	return md5str
}

func Register(c *gin.Context) {
	json := make(map[string]string)
	c.BindJSON(&json)

	grade, err := strconv.ParseInt(json["grade"], 10, 16)
	if err != nil {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}
	if grade != 20190 && grade != 20200 && grade != 20210 && grade != 20191 && grade != 20201 && grade != 20211 {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}

	class, err := strconv.ParseInt(json["class"], 10, 8)
	if err != nil {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}
	if class > 8 || class < 1 {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}

	number, err := strconv.ParseInt(json["number"], 10, 8)
	if err != nil {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}
	if number > 50 || number < 1 {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}

	name := json["name"]
	if name == "" {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}

	password := json["password"]
	if password == "" {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}

	student := &Student{
		Grade:  int16(grade),
		Class:  int8(class),
		Number: int8(number),
	}
	has, _ := engine.Get(student)
	if has {
		c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
		return
	}

	token := uuid.New().String()
	c.SetCookie("token", token, 3600, "/", "", false, true)
	student.Name = name
	student.Token = token
	student.Password = MD5(password)

	_, err = engine.Insert(student)
	if err != nil {
		panic(err)
	}

	c.String(http.StatusOK, token)
}

func Login(c *gin.Context) {
	json := make(map[string]string)
	c.BindJSON(&json)

	grade, err := strconv.ParseInt(json["grade"], 10, 16)
	if err != nil {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}
	if grade != 20190 && grade != 20200 && grade != 20210 && grade != 20191 && grade != 20201 && grade != 20211 {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}

	class, err := strconv.ParseInt(json["class"], 10, 8)
	if err != nil {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}
	if class > 8 || class < 1 {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}

	number, err := strconv.ParseInt(json["number"], 10, 8)
	if err != nil {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}
	if number > 50 || number < 1 {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}

	password := json["password"]
	if password == "" {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}

	student := &Student{
		Grade:  int16(grade),
		Class:  int8(class),
		Number: int8(number),
	}
	has, _ := engine.Get(student)
	if !has {
		c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
		return
	}

	if MD5(password) != student.Password {
		c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
		return
	}

	token := uuid.New().String()
	c.SetCookie("token", token, 3600, "/", "", false, true)
	student.Token = token
	engine.ID(student.Id).Update(student)

	c.String(http.StatusOK, token)
}

func Logout(c *gin.Context) {
	token, err := c.Cookie("token")

	// no token in cookie
	if err != nil {
		c.String(http.StatusOK, http.StatusText(http.StatusOK))
		return
	}

	// delete cookie
	c.SetCookie("token", "token", -1, "/", "", false, true)

	var student Student
	has, _ := engine.Where("token = ?", token).Get(&student)

	// no such user
	if !has {
		c.String(http.StatusOK, http.StatusText(http.StatusOK))
		return
	}

	// delete token in DB
	student.Token = "token"
	engine.Where("id = " + strconv.FormatInt(student.Id, 10)).Update(&student)

	c.String(http.StatusOK, http.StatusText(http.StatusOK))
}

func Info(c *gin.Context) {
	student := getUser(c)
	if student == nil {
		c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
		return
	}

	clubName := ""
	selection := &ClubSelection{Student: student.Id}
	has, _ := engine.Get(selection)
	if has {
		club := &Club{Id: selection.Club}
		engine.Get(club)
		clubName = club.Name
	}

	orders := make([]*Order, 0)
	engine.Find(&orders, &Order{Student: student.Id})

	readableOrders := make([]gin.H, 0)

	var product *Product
	for _, o := range orders {
		product = &Product{Id: o.Product}
		engine.Get(product)
		readableOrders = append(readableOrders, gin.H{
			"Product": product.Name,
			"Code":    o.Code,
			"Time":    o.Time.Format("2006-01-02 15:04:05"),
			"Status":  o.Status,
		})
	}

	c.JSON(201, gin.H{
		"Name":   student.Name,
		"Club":   clubName,
		"Orders": readableOrders,
	})
}

func GetClubs(c *gin.Context) {
	student := getUser(c)
	if student == nil {
		c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
		return
	}

	clubSelection := &ClubSelection{Student: student.Id}
	engine.Get(clubSelection)
	clubs := make([]*Club, 0)
	engine.Find(&clubs)
	c.JSON(200, gin.H{
		"Clubs":    clubs,
		"Selected": clubSelection.Club,
	})
}

func Select(c *gin.Context) {
	student := getUser(c)
	if student == nil {
		c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
		return
	}

	selected := c.Query("club")
	clubId, _ := strconv.ParseInt(selected, 10, 64)
	club := &Club{Id: clubId}
	has, _ := engine.Get(club)

	if !has {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}

	selection := &ClubSelection{Student: student.Id}
	has, _ = engine.Get(selection)
	if has {
		selection.Club = clubId
		engine.ID(selection.Student).Update(selection)
	} else {
		selection := &ClubSelection{Student: student.Id, Club: clubId}
		engine.Insert(selection)
	}
	c.String(http.StatusCreated, http.StatusText(http.StatusCreated))
}

func GetProducts(c *gin.Context) {
	student := getUser(c)
	if student == nil {
		c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
		return
	}

	products := make([]*Product, 0)
	engine.Find(&products)
	c.JSON(http.StatusOK, products)
}

func Purchase(c *gin.Context) {
	student := getUser(c)
	if student == nil {
		c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
		return
	}

	productID, err := strconv.ParseInt(c.Query("product"), 10, 64)

	if err != nil {
		c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}

	product := Product{
		Id: productID,
	}
	has, _ := engine.Get(&product)

	if !has {
		c.String(http.StatusNotFound, http.StatusText(http.StatusNotFound))
		return
	}

	const pool = "ABCDEFGHJKLMNPQRSTUVWXYZ"
	rand.Seed(time.Now().UnixNano())
	var code string

	for {
		b := make([]byte, 4)
		for i := range b {
			b[i] = pool[rand.Intn(len(pool))]
		}
		code = string(b)

		has, _ = engine.Get(&Order{Code: code})
		if !has {
			break
		}
	}

	order := Order{
		Student: student.Id,
		Product: productID,
		Status:  OrderUnpaid,
		Code:    code,
	}

	_, err = engine.Insert(&order)

	if err != nil {
		c.String(http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError))
	}

	c.JSON(200, gin.H{
		"code":    code,
		"product": product,
	})
}

func CancelOrder(c *gin.Context) {
	student := getUser(c)
	if student == nil {
		c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
		return
	}

	code := c.Query("code")

	if code != "" {
		order := &Order{Code: code}
		has, _ := engine.Get(order)

		if !has {
			c.String(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
			return
		}

		if order.Student != student.Id {
			c.String(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
			return
		}

		engine.ID(order.Id).Delete(order)

		c.String(http.StatusOK, http.StatusText(http.StatusOK))
		return
	}
}
